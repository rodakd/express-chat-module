const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const dotenv = require('dotenv');
const db = require('./models');

dotenv.config();
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));

app.use(require('./routes/room.routes'));
app.use(require('./routes/user.routes'));
app.use(require('./routes/message.routes'));

app.use((req, res) => {
  res.status(404).send({
    message: 'Page not found',
  });
});

app.use((err, req, res, next) => {
  res.status(500).send({ message: err.message });
});

db.sequelize.sync();

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
