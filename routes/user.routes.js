const express = require('express');
const user = require('../controllers/user.controller');
const validateFields = require('../middleware/validateFields');
const verifyAuth = require('../middleware/verifyAuth');

const router = express.Router();

router.post(
  '/api/chat/signup',
  verifyAuth.checkDuplicateEmail,
  validateFields.signup,
  user.signup
);

router.post('/api/chat/signin', validateFields.signin, user.signin);

module.exports = router;
