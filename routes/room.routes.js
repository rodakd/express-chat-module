const express = require('express');
const room = require('../controllers/room.controller');
const verifyAuth = require('../middleware/verifyAuth');
const getters = require('../middleware/getters');

const router = express.Router();

router.post('/api/chat/rooms', verifyAuth.checkToken, room.create);

router.get('/api/chat/rooms', verifyAuth.checkToken, room.getAll);

router.delete(
  '/api/chat/rooms/:roomId',
  verifyAuth.checkToken,
  getters.getRoomOr404,
  verifyAuth.isUserRoomAdmin,
  room.delete
);

router.put(
  '/api/chat/rooms/:roomId',
  verifyAuth.checkToken,
  getters.getRoomOr404,
  verifyAuth.isUserRoomAdmin,
  room.update
);

router.post(
  '/api/chat/rooms/:roomId/users',
  verifyAuth.checkToken,
  getters.getRoomOr404,
  verifyAuth.isUserRoomAdmin,
  getters.getUserOr404,
  room.addUser
);

router.get(
  '/api/chat/rooms/:roomId/users',
  verifyAuth.checkToken,
  getters.getRoomOr404,
  verifyAuth.isUserInRoom,
  room.getParticipants
);

module.exports = router;
