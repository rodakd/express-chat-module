const express = require('express');
const message = require('../controllers/message.controller');
const verifyAuth = require('../middleware/verifyAuth');
const getters = require('../middleware/getters');

const router = express.Router();

router.get(
  '/api/chat/rooms/:roomId/messages',
  verifyAuth.checkToken,
  getters.getRoomOr404,
  verifyAuth.isUserInRoom,
  message.getAll
);

router.post(
  '/api/chat/rooms/:roomId/messages',
  verifyAuth.checkToken,
  getters.getRoomOr404,
  verifyAuth.isUserInRoom,
  message.addMessage
);

module.exports = router;
