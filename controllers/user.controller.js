const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const config = require('../config/auth.config');
const { User, UserToken } = require('../models');
const handleException = require('../middleware/handleException');

exports.signup = handleException(async (req, res) => {
  await User.create({
    name: req.body.name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
  });
  res.status(200).send();
});

exports.signin = handleException(async (req, res) => {
  const user = await User.findOne({
    where: { email: req.body.email },
  });
  if (!user) return res.status(401).send({ message: 'Invalid password or email' });
  const passwordIsValid = checkPasswordValid(req.body.password, user.password);
  if (!passwordIsValid) return res.status(401).send({ message: 'Invalid password or email' });
  if (req.body.token) await createTokenIfNotInDatabase(req.body.token, user.id);
  const responseToken = jwt.sign({ id: user.id }, config.secret, {});
  res.status(200).send({ userId: user.id, token: responseToken });
});

const checkPasswordValid = (requestPassword, userPassword) => {
  const isValid = bcrypt.compareSync(requestPassword, userPassword);
  if (!isValid) return false;
  return true;
};

const createTokenIfNotInDatabase = async (token, userId) => {
  const foundToken = await UserToken.findOne({
    where: {
      token,
    },
  });
  if (foundToken) return;
  await UserToken.create({
    token,
    userId,
  });
};
