const { Room, Participant } = require('../models');
const handleException = require('../middleware/handleException');

exports.getAll = handleException(async (req, res) => {
  const { signedInUser } = req;
  const rooms = await signedInUser.getRooms();
  if (rooms.length === 0) return res.status(404).send({ message: 'Rooms not found' });
  const prettierResponse = [];
  rooms.forEach((room) => {
    const { id, name, avatarPath } = room;
    prettierResponse.push({ id, name, avatarPath });
  });
  return res.status(200).send(prettierResponse);
});

exports.create = handleException(async (req, res) => {
  const { signedInUser, body } = req;
  const room = await Room.create({
    name: body.name,
  });
  await signedInUser.addRoom(room, {
    through: { isAdmin: true, nickname: signedInUser.name },
  });
  res.status(200).send({ roomId: room.id });
});

exports.delete = handleException(async (req, res) => {
  const { room } = req;
  await room.destroy();
  return res.status(200).send();
});

exports.update = handleException(async (req, res) => {
  const { room, body } = req;
  await room.update(body);
  return res.status(200).send();
});

exports.addUser = handleException(async (req, res) => {
  const { requestedUser, room } = req;
  if (await checkUserAlreadyInRoom(requestedUser.id, room.id))
    return res.status(400).send({ message: 'User is already in this room' });
  await room.addUser(requestedUser, {
    through: { nickname: requestedUser.name },
  });
  return res.status(200).send();
});

const checkUserAlreadyInRoom = async (userId, roomId) => {
  const user = await Participant.findOne({
    where: {
      userId,
      roomId,
    },
  });
  if (!user) return false;
  return true;
};

exports.getParticipants = async (req, res) => {
  const { room } = req;
  const participants = await Participant.findAll({
    attributes: ['userId', 'nickname', 'isAdmin'],
    where: {
      roomId: room.id,
    },
  });
  if (participants.length === 0) return res.status(404).send({ message: 'Participants not found' });
  return res.status(200).send(participants);
};
