const { Message, User } = require('../models');
const handleException = require('../middleware/handleException');

exports.getAll = handleException(async (req, res) => {
  const { room } = req;
  const messages = await Message.findAll({
    attributes: ['id', 'type', 'body', 'createdAt'],
    where: {
      roomId: room.id,
    },
    include: {
      model: User,
      attributes: ['id', 'name', 'avatarPath'],
    },
    order: [['createdAt', 'DESC']],
  });
  if (messages.length === 0) return res.status(404).send({ message: 'Messages not found' });
  return res.status(200).send(messages);
});

exports.addMessage = handleException(async (req, res) => {
  const { signedInUser, room, body } = req;
  await Message.create({
    type: body.type,
    body: body.body,
    roomId: room.id,
    userId: signedInUser.id,
  });
  return res.status(200).send();
});
