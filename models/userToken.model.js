module.exports = (sequelize, Sequelize) => {
  const UserToken = sequelize.define('user_token', {
    token: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  });

  return UserToken;
};
