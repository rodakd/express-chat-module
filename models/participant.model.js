module.exports = (sequelize, Sequelize) => {
  const Participant = sequelize.define(
    'participants',
    {
      isAdmin: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      nickname: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    { timestamps: false }
  );

  return Participant;
};
