module.exports = (sequelize, Sequelize) => {
  const Message = sequelize.define('messages', {
    type: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    body: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  });

  return Message;
};
