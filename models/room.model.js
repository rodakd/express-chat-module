module.exports = (sequelize, Sequelize) => {
  const Room = sequelize.define('rooms', {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    avatarPath: {
      type: Sequelize.STRING,
      allowNull: true,
    },
  });

  return Room;
};
