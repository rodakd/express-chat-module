const Sequelize = require('sequelize');
const config = require('../config/db.config');

const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  operatorsAliases: 0,
  define: {
    charset: 'utf8',
    collate: 'utf8_general_ci',
    timestamps: true,
  },
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.Message = require('./message.model')(sequelize, Sequelize);
db.Room = require('./room.model')(sequelize, Sequelize);
db.User = require('./user.model')(sequelize, Sequelize);
db.UserToken = require('./userToken.model')(sequelize, Sequelize);
db.Participant = require('./participant.model')(sequelize, Sequelize);

db.User.hasMany(db.Message);
db.Message.belongsTo(db.User);

db.Room.hasMany(db.Message);
db.Message.belongsTo(db.Room);

db.User.belongsToMany(db.Room, { through: db.Participant });
db.Room.belongsToMany(db.User, { through: db.Participant });

db.User.hasMany(db.UserToken);
db.UserToken.belongsTo(db.User);

module.exports = db;
