const { validate } = require('../services/validator.service');

exports.signup = (req, res, next) => {
  const validationRule = {
    email: 'required|email',
    password: 'required|string|min:6',
    name: 'required|string',
  };
  validate(req.body, validationRule, res, next);
};

exports.signin = (req, res, next) => {
  const validationRule = {
    email: 'required|email',
    password: 'required|string',
  };
  validate(req.body, validationRule, res, next);
};
