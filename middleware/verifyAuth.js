const jwt = require('jsonwebtoken');
const { User, Participant } = require('../models');
const config = require('../config/auth.config.js');
const handleException = require('./handleException');

exports.checkToken = handleException((req, res, next) => {
  const token = req.headers['x-access-token'];
  if (!token) {
    return res.status(403).send({
      message: 'Nie podano tokenu',
    });
  }
  jwt.verify(token, config.secret, async (err, decoded) => {
    const user = await User.findOne({
      where: {
        id: decoded.id,
      },
    });
    if (!user || err) {
      return res.status(401).send({ message: 'Unauthorized!' });
    }
    req.signedInUser = user;
    next();
  });
});

exports.checkDuplicateEmail = handleException(async (req, res, next) => {
  const user = await User.findOne({
    where: {
      email: req.body.email,
    },
  });
  if (user) return res.status(422).send({ message: 'This email exists!' });
  next();
});

const getAdminsIDs = async (roomId) => {
  return Participant.findAll({
    attributes: ['userId'],
    where: {
      roomId,
      isAdmin: true,
    },
  });
};

exports.isUserRoomAdmin = async (req, res, next) => {
  const { signedInUser, room } = req;
  const adminsIDs = await getAdminsIDs(room.id);
  let found = false;
  for (let i = 0; i < adminsIDs.length; i += 1)
    if (adminsIDs[i].userId === signedInUser.id) {
      found = true;
      break;
    }
  if (!found) return res.status(401).send({ message: "You're not an admin" });
  next();
};

exports.isUserInRoom = async (req, res, next) => {
  const { signedInUser, room } = req;
  const participant = Participant.findOne({
    where: {
      userId: signedInUser.id,
      roomId: room.id,
    },
  });
  if (!participant) return res.status(401).send({ message: "You're not in this room" });
  next();
};
