const { Room, User } = require('../models');
const handleException = require('./handleException');

exports.getRoomOr404 = handleException(async (req, res, next) => {
  const { params, body } = req;
  const room = await Room.findOne({
    where: {
      id: params.roomId || body.roomId,
    },
  });
  if (!room) return res.status(404).send({ message: 'Room not found' });
  req.room = room;
  next();
});

exports.getUserOr404 = handleException(async (req, res, next) => {
  const { params, body } = req;
  const user = await User.findOne({
    where: {
      id: params.userId || body.userId,
    },
  });
  if (!user) return res.status(404).send({ message: 'User not found' });
  req.requestedUser = user;
  next();
});
